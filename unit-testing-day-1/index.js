const express = require('express');
const app = express();
const port = 5000;

app.use(express.json());

const newUser = {
    firstName: 'John',
    lastName: 'Dela Cruz',
    age: 18,
    contactNumber: '09123456789',
    batchNumber: 151,
    email: 'sample@email.com',
    password: 'thisisasamplepassword'
}

app.listen(port, () => console.log(`Server is Running on Port ${port}`));


module.exports = {
    newUser: newUser
}