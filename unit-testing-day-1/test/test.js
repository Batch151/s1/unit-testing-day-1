const {assert} = require('chai');
const {newUser} = require('../index.js');

//describe gives structure to your test suite.
describe('Test newUser object', () => {
    it('Assert newUser type is object', () => {
        assert.equal(typeof(newUser), 'object')
    });
    it('Assert newUser.email is type string', () => {
        assert.equal(typeof(newUser.email), 'string')
    });
    it('Assert newUser.email is not undefined', () => {
        assert.notEqual(typeof(newUser.email), 'undefined')
    });
    it('Assert newUser.password is type string', () => {
        assert.equal(typeof(newUser.password), 'string')
    });
    it('Assert that password is 16 characters long', () => {
        assert.isAtLeast(newUser.password.length, 16)
    });
});

describe('Activity', () => {
    it('(1) Assert that newUser firstName type is string', () => {
        assert.equal(typeof(newUser.firstName), 'string')
    });
    it('(2) Assert that newUser lastName type is string', () => {
        assert.equal(typeof(newUser.lastName), 'string')
    });
    it('(3) Assert that newUser firstName type is not undefined', () => {
        assert.notEqual(typeof(newUser.firstName), 'undefined')
    });
    it('(4) Assert that newUser lastName type is not undefined', () => {
        assert.notEqual(typeof(newUser.lastName), 'undefined')
    });
    it('(5) Assert that newUser age is at least 18', () => {
        assert.isAtLeast(newUser.age, 18)
    })
    it('(6) Assert that newUser age type is a number', () => {
        assert.equal(typeof(newUser.age), 'number')
    })
    it('(7) Assert that newUser contactNumber type is string', () => {
        assert.equal(typeof(newUser.contactNumber), 'string')
    })
    it('(8) Assert that newUser batchNumber type is number', () => {
        assert.equal(typeof(newUser.batchNumber), 'number')
    })
    it('(9) Assert that newUser batchNumber type is not undefined', () => {
        assert.notEqual(typeof(newUser.batchNumber), 'undefined')
    })
})